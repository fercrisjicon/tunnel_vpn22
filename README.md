# Teoria
--------
#### Modelo 01

------------------------
====================================================
------------------------
# Practicas
-----------
- instalar openvpn en ambos puntos (Origen y Destino)
  > sudo apt-get update
  > sudo apt-get install openvpn 
- encerder servidor openvpn
  > sudo systemctl start openvpn
  > sudo systemctl status openvpn
  
## Practica 1: Tunel Host to Host [no cifrado]
Agarramos dos ordenadores de la misma LAN o por separados por internet(dos de la aula).
1. desde nuestro ordenador(local) al remoto:
> sudo openvpn --remote i15 --dev tun1 --ifconfig 10.4.0.1 10.4.0.2 --verb 9

2. desde el otro ordenador(remoto) al local:
> sudo openvpn --remote i10 --dev tun1 --ifconfig 10.4.0.2 10.4.0.1 --verb 9

3. en otra terminal, desde el local:
   3.1. hacer ping
   > ping 10.4.0.2
    ```
    PING 10.4.0.2 (10.4.0.2) 56(84) bytes of data.
    64 bytes from 10.4.0.2: icmp_seq=1 ttl=64 time=1.82 ms
    64 bytes from 10.4.0.2: icmp_seq=2 ttl=64 time=1.76 ms
    64 bytes from 10.4.0.2: icmp_seq=3 ttl=64 time=2.12 ms
    64 bytes from 10.4.0.2: icmp_seq=4 ttl=64 time=1.31 ms
    ^C
    --- 10.4.0.2 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3005ms
    rtt min/avg/max/mdev = 1.311/1.749/2.117/0.288 ms
    ```
    3.2. ver que hemos creado el tunel
    > ip a
    ```
    8: tun1: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 500
    link/none 
    inet 10.4.0.1 peer 10.4.0.2/32 scope global tun1
           valid_lft forever preferred_lft forever
    inet6 fe80::fac4:4628:64dc:1537/64 scope link stable-privacy 
       valid_lft forever preferred_lft forever
    ```

4. en otra terminal, desde el remoto:
   4.1. hacer ping
   > ping 10.4.0.1
    ```
    PING 10.4.0.1 (10.4.0.1) 56(84) bytes of data.
    64 bytes from 10.4.0.1: icmp_seq=1 ttl=64 time=1.46 ms
    64 bytes from 10.4.0.1: icmp_seq=2 ttl=64 time=2.18 ms
    64 bytes from 10.4.0.1: icmp_seq=3 ttl=64 time=1.40 ms
    64 bytes from 10.4.0.1: icmp_seq=4 ttl=64 time=1.53 ms
    ^C
    --- 10.4.0.1 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3005ms
    rtt min/avg/max/mdev = 1.395/1.641/2.180/0.314 ms
    ```
    4.2. ver que hemos creado el tunel
    > ip a
    ```
    21: tun1: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 500
    link/none 
    inet 10.4.0.2 peer 10.4.0.1/32 scope global tun1
       valid_lft forever preferred_lft forever
    inet6 fe80::1df0:35fa:220c:4206/64 scope link stable-privacy 
       valid_lft forever preferred_lft forever
    ```

## Practica 1: Tunel Host to Host [pre-shared key]
1. montar una clave static en los dos ordenadores
> sudo openvpn --genkey secret key_barcelona

2. copia la clave al remoto:
    2.1. desde el remoto
    > nc -l 1234 > key_barcelona
    
    2.2. desde el local (como root)
    > nc -w 3 i15 1234 < key_barcelona

3. abrir el tunel
    2.1. local --> remoto: 
    > sudo openvpn --remote i15 --dev tun1 --ifconfig 10.4.0.1 10.4.0.2 --verb 9 --secret key_barcelona
    
    2.2. remoto --> local:
    > sudo openvpn --remote i10 --dev tun1 --ifconfig 10.4.0.2 10.4.0.1 --verb 9 --secret key_barcelona

4. en otra terminal, desde el local:
   3.1. hacer ping
   > ping 10.4.0.2
    ```
    PING 10.4.0.2 (10.4.0.2) 56(84) bytes of data.
    64 bytes from 10.4.0.2: icmp_seq=1 ttl=64 time=1.67 ms
    64 bytes from 10.4.0.2: icmp_seq=2 ttl=64 time=1.73 ms
    64 bytes from 10.4.0.2: icmp_seq=3 ttl=64 time=1.87 ms
    64 bytes from 10.4.0.2: icmp_seq=4 ttl=64 time=2.22 ms
    ^C
    --- 10.4.0.2 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3006ms
    rtt min/avg/max/mdev = 1.671/1.871/2.215/0.211 ms
    ```

5. en otra terminal, des del remoto:
   4.1. hacer ping
   > ping 10.4.0.1
    ```
    PING 10.4.0.1 (10.4.0.1) 56(84) bytes of data.
    64 bytes from 10.4.0.1: icmp_seq=1 ttl=64 time=2.16 ms
    64 bytes from 10.4.0.1: icmp_seq=2 ttl=64 time=2.68 ms
    64 bytes from 10.4.0.1: icmp_seq=3 ttl=64 time=2.73 ms
    64 bytes from 10.4.0.1: icmp_seq=4 ttl=64 time=3.01 ms
    ^C
    --- 10.4.0.1 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3005ms
    rtt min/avg/max/mdev = 2.162/2.645/3.005/0.304 ms
    ```

## Practica 1: Tunel Host to Host [TLS Public/Secret Key]
1. h
2. f
3. g
4. h
5. j

## Practica 1: Tunel Network to Network